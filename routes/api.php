<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProductController;

Route::post('/login', [AuthController::class,'login'])->name('login.api');
Route::post('/register', [AuthController::class,'register'])->name('register.api');
Route::middleware('auth:api')->group(function () {
    // our routes to be protected will go in here
    Route::post('/logout', [AuthController::class,'logout'])->name('logout.api');
    Route::get('/products', [ProductController::class,'index'])->name('product.list');
    Route::post('/products/upload',  [ProductController::class,'upload'])->name('product.upload');
});
