<?php

namespace App\Http\Controllers\Api;

use App\Http\Repositories\UserRepository;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\UserCollection;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    private $userRepository;

    /**
     * AuthController constructor.
     * @created by rahul on (08 Dec 2022 at 3:14 pm)
     */
    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * DESCRIPTION : This Function user login
     * @param LoginRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahulon (08 Dec 2022 at 3:14 pm)
     */
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                return response()->json([
                    'status' => true,
                    'message' => 'Password matched',
                    'data' => [ 'user' => new UserCollection($user),'token' => $user->createToken('api')->accessToken ]
                ], 200);
            } else {
                return response()->json([
                    'status' => false,
                    'message' => 'Password mismatch',
                    'errors' => null
                ], 422);
            }
        } else {
            return response()->json([
                'status' => false,
                'message' => 'User does not exist',
                'errors' => null
            ], 422);;
        }
    }


    /**
     * DESCRIPTION : This Function user register
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahul on (08 Dec 2022 at 2:59 pm)
     */
    public function register(UserRegisterRequest $request)
    {
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = $this->userRepository->createUser($request->toArray());
        return response()->json([
            'status' => true,
            'message' => 'User Registered successfully!',
            'data' => [ 'user' => new UserCollection($user),'token' => $user->createToken('api')->accessToken ]
        ], 200);
    }

    /**
     * DESCRIPTION : This Function user logout
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahul@protracked.in on (08 Dec 2022 at 3:15 pm)
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        return response()->json([
            'status' => true,
            'message' => 'You have been successfully logged out!',
            'data' => null
        ], 200);
    }
}
