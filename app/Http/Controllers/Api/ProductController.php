<?php

namespace App\Http\Controllers\Api;

use App\Http\Repositories\ProductRepository;
use App\Http\Resources\ProductCollection;
use App\Http\Requests\FileUploadRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{

    private $productRepository;

    /**
     * ProductController constructor.
     * @created by rahul on (08 Dec 2022 at 3:14 pm)
     */
    public function __construct()
    {
        $this->productRepository = new ProductRepository();
    }

    /**
     * DESCRIPTION : This Function products
     * @param LoginRequest $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahulon (08 Dec 2022 at 3:14 pm)
     */
    public function index(Request $request)
    {
        $data = ProductCollection::collection($this->productRepository->getAllProducts());
        return response()->json([
            'status' => true,
            'message' => 'Data fetched',
            'data' => $data
        ], 200);;

    }


    /**
     * DESCRIPTION : This Function products uploads
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahul on (08 Dec 2022 at 2:59 pm)
     */
    public function upload(FileUploadRequest $request)
    {
        $file = $request->file('file');
        $name = '/files/' . uniqid() . '.' . $file->extension();
        $import = Excel::import(new ProductImport(),
            $request->file('file')->storePubliclyAs('public', $name));

        return response()->json([
            'status' => true,
            'message' => 'Product data upload successfully!',
            'data' => explode('.', $request->file->getClientOriginalName())
        ], 200);


    }

    /**
     * DESCRIPTION : This Function user logout
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     * @Created by rahul@protracked.in on (08 Dec 2022 at 3:15 pm)
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        return response()->json([
            'status' => true,
            'message' => 'You have been successfully logged out!',
            'data' => null
        ], 200);
    }
}
