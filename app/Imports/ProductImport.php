<?php

/**
 * @Description : This File
 * @Created     By rahul@protracked.in on (08 Dec 2022 at 6:10 pm)
 */

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithMappedCells;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductImport implements ToModel, WithChunkReading,WithHeadingRow, WithBatchInserts,WithValidation
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
         Product::updateOrCreate([
            'name'  => $row['name'],
            'price' => $row['price'],
            'sku'    => $row['sku'],
            'description'    => $row['description'],
        ]);
    }

    /**
     * DESCRIPTION : This Function
     * @return int
     * @Created by rahul on (08 Dec 2022 at 7:37 pm)
     */
    public function batchSize(): int
    {
        return 1000;
    }

    /**
     * DESCRIPTION : This Function chunkSize
     * @return int
     * @Created by rahul on (08 Dec 2022 at 7:58 pm)
     */
    public function chunkSize(): int
    {
        return 1000;
    }

    /**
     * DESCRIPTION : This Function
     * @return string[]
     * @Created by rahul on (08 Dec 2022 at 7:58 pm)
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:200',
            'price' => 'required|numeric',
            'sku' => 'required|string|max:100',
            'description' => 'required|string|max:1000',

        ];
    }





}
